
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
	var merilec = $("#dodajVitalnoMerilec").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiEhrIdZaVitalneZnake").val();
	console.log(tip);
	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				if (tip == "krvni tlak") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Krvni tlak</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + "Sistolicni tlak: " + res[i].systolic + "mm[Hg]" + "<br>" + " Diastolicni tlak: " + res[i].diastolic + "mm[Hg]";
                          " " + res[i].unit + "</td></tr>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "telesna teža") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
					    		console.log(res);
					    		console.log(res[0].weight);
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].weight + " " 	+
                          res[i].unit + "</td></tr>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "višina") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
					    		console.log(res);
					    		console.log(res[0].weight);
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].height + "cm" + "</td></tr>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

function vreme(){
   
     var cityName = $("#vremeInput").val();
     console.log(cityName);
     var weatherUrl = "https://api.apixu.com/v1/current.json?key=1c6a3cc1329642aaac7205823172705&q="+cityName;
     
     if(!cityName || cityName.trim().length==0){
     }else{
        $.ajax({
         url: weatherUrl,
         type: 'GET',
		 success: function(res){
			console.log("ok");
		     
		     var results = "<table class='table table-striped " +
                           "table-hover'><tr><th align='center'>Information</th>" +
                           "<th></th></tr>";
                           
			results += "<tr><td><b>City</b> "+ res.location.name+ 
                       "</td><td><b>Country</b> "+res.location.country+"</td>";
            results += "<tr><td><b>Current Weather</b> "+res.current.condition.text+ 
                       "</td><td><b>Humidity</b> "+res.current.humidity+"%</td>";
            results += "<tr><td><b>Temperature</b> "+ res.current.temp_c+ 
                       " °C</td><td><b>Wind speed</b> "+res.current.wind_kph+" km/h</td>";
                            
           results += "</table>";
           $("#podatkiVreme").empty();
           $("#podatkiVreme").append("Temperatura: " + res.current.temp_c + "°C Vlaga: " + res.current.humidity + "%");
		     
		 },
		 error: function(err){
		 	console.log("ni ok");
		     $("#supportMessage_weather").html("<span class='" +
            "label label-danger fade-in'>Not a valid city.</span></br>");
		 }
     });   
     }
}

 var ehrIdOseb = new Array(3);
 var os = 0;
 
  function generirajPodatke(oseba,callback){
     sessionId = getSessionId();

	var ime = oseba[0];
	var priimek = oseba[1];
  var datumRojstva = oseba[2].substring(0, 10);

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        console.log(ehrId);
		        callback(ehrId);
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		            },
		            error: function(err) {
		            }
		        });
		    }
		});
	}
}
 
 function generiraj1(eID,arr,i,j){
   
   sessionId = getSessionId();

        var height = arr[j][1][i];
        var dateInput = arr[j][2][i];
        var weight = arr[j][0][i];

       $.ajaxSetup({
             headers: {"Ehr-Session": sessionId}
          });
       var podatki = {
               "ctx/language": "en",
               "ctx/territory": "SI",
               "ctx/time": dateInput,
               "vital_signs/height_length/any_event/body_height_length": height,
               "vital_signs/body_weight/any_event/body_weight": weight,
               "vital_signs/body_temperature/any_event/temperature|magnitude": 0,
		        "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		        "vital_signs/blood_pressure/any_event/systolic": 0,
		        "vital_signs/blood_pressure/any_event/diastolic": 0,
		        "vital_signs/indirect_oximetry:0/spo2|numerator": 0
       };
       
       var parametriZahteve = {
           ehrId: eID,
           templateId: 'Vital Signs',
           format: 'FLAT'
         };
       
       $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki), 
            
             success: function (res) {
             },
             error: function(err) {
             }
       });   
 }
 
 function generiraj2(eID,arr,i,j){
     sessionId = getSessionId();

	var datumInUra = arr[j][2][i];
	var telesnaTemperatura = arr[j][3][i];
	var sistolicniKrvniTlak = arr[j][4][i];
	var diastolicniKrvniTlak = arr[j][5][i];
	var nasicenostKrviSKisikom = arr[j][6][i];

        $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
        });
        
        var podatki = {
            "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        };
        var parametriZahteve = {
		    ehrId: eID,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
        };
        $.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		    },
		    error: function(err) {
		    }
        });
}
 
 function TriStiriGeneriri(){
    	
    var oseba = new Array(3);
    var arr = new Array(3);
     for(var i = 0;i<3;i++){
        oseba[i]=new Array(3);    
        arr[i]=new Array(7);
       
        for(var j = 0;j<7;j++){
            arr[i][j]=new Array(5);
     }
}
     
     /*MY TIME TO SHINE BABY*/
     oseba[0][0] = 'Matej';
     oseba[0][1] = 'Janežič';
     oseba[0][2] = '1998-05-07T14:03';
     arr[0][0][0] = '150'; arr[0][0][1] = '140'; arr[0][0][2] = '130'; arr[0][0][3] = '120'; arr[0][0][4] = '110';
     arr[0][1][0] = '195'; arr[0][1][1] = '197'; arr[0][1][2] = '199'; arr[0][1][3] = '201'; arr[0][1][4] = '207';
     arr[0][2][0] = '2015-04-10T11:08'; arr[0][2][1] = '2015-06-10T11:09'; arr[0][2][2] = '2015-08-10T13:08'; arr[0][2][3] = '2015-09-10T12:11'; arr[0][2][4] = '2015-11-05T22:18';
     arr[0][3][0] = '36'; arr[0][3][1] = '37'; arr[0][3][2] = '37'; arr[0][3][3] = '38'; arr[0][3][4] = '37';
     arr[0][4][0] = '100'; arr[0][4][1] = '123'; arr[0][4][2] = '120'; arr[0][4][3] = '111'; arr[0][4][4] = '115';
     arr[0][5][0] = '86'; arr[0][5][1] = '87'; arr[0][5][2] = '90'; arr[0][5][3] = '84'; arr[0][5][4] = '88';
     arr[0][6][0] = '90'; arr[0][6][1] = '95'; arr[0][6][2] = '97'; arr[0][6][3] = '92'; arr[0][6][4] = '95';
     
     oseba[1][0] = 'Daniel';
     oseba[1][1] = 'Vitas';
     oseba[1][2] = '1965-09-08T13:08';
     arr[1][0][0] = '75'; arr[1][0][1] = '65'; arr[1][0][2] = '69'; arr[1][0][3] = '80'; arr[1][0][4] = '78';
     arr[1][1][0] = '159'; arr[1][1][1] = '160'; arr[1][1][2] = '166'; arr[1][1][3] = '166'; arr[1][1][4] = '165';
     arr[1][2][0] = '2014-02-10T11:08'; arr[1][2][1] = '2015-03-10T11:09'; arr[1][2][2] = '2016-08-10T13:08'; arr[1][2][3] = '2017-09-10T12:11'; arr[1][2][4] = '2017-11-05T22:18';
     arr[1][3][0] = '34.6'; arr[1][3][1] = '37'; arr[1][3][2] = '37.2'; arr[1][3][3] = '38'; arr[1][3][4] = '37.1';
     arr[1][4][0] = '108'; arr[1][4][1] = '101'; arr[1][4][2] = '100'; arr[1][4][3] = '107'; arr[1][4][4] = '110';
     arr[1][5][0] = '95'; arr[1][5][1] = '96'; arr[1][5][2] = '93'; arr[1][5][3] = '92'; arr[1][5][4] = '90';
     arr[1][6][0] = '95'; arr[1][6][1] = '91'; arr[1][6][2] = '97'; arr[1][6][3] = '98'; arr[1][6][4] = '92';
     
     oseba[2][0] = 'Peter';
     oseba[2][1] = 'Knez';
     oseba[2][2] = '1998-11-22T15:08';
     arr[2][0][0] = '71'; arr[2][0][1] = '72'; arr[2][0][2] = '67'; arr[2][0][3] = '66'; arr[2][0][4] = '65';
     arr[2][1][0] = '159'; arr[2][1][1] = '165'; arr[2][1][2] = '171'; arr[2][1][3] = '180'; arr[2][1][4] = '184';
     arr[2][2][0] = '2015-02-10T11:08'; arr[2][2][1] = '2016-03-10T11:09'; arr[2][2][2] = '2017-08-10T13:08'; arr[2][2][3] = '2018-01-10T12:11'; arr[2][2][4] = '2018-12-05T22:18';
     arr[2][3][0] = '36.7'; arr[2][3][1] = '37.3'; arr[2][3][2] = '37'; arr[2][3][3] = '38'; arr[2][3][4] = '37.9';
     arr[2][4][0] = '102'; arr[2][4][1] = '123'; arr[2][4][2] = '121'; arr[2][4][3] = '115'; arr[2][4][4] = '119';
     arr[2][5][0] = '86'; arr[2][5][1] = '90'; arr[2][5][2] = '89'; arr[2][5][3] = '85'; arr[2][5][4] = '90';
     arr[2][6][0] = '91'; arr[2][6][1] = '92'; arr[2][6][2] = '93'; arr[2][6][3] = '92'; arr[2][6][4] = '91';
     
    generirajPodatke(oseba[0],function(rez){
        ehrIdOseb[os%3]=rez;
        os++;
    });
   generirajPodatke(oseba[1],function(rez){
        ehrIdOseb[os%3]=rez;
        os++;
    });
    generirajPodatke(oseba[2],function(rez){
        ehrIdOseb[os%3]=rez;
        os++;
    });
    
    setTimeout(function(){ 
            for(var i = 0;i<5;i++){
            generiraj1(ehrIdOseb[0],arr,i,0); 
            generiraj2(ehrIdOseb[0],arr,i,0);
            }
    }, 1000);
    
    setTimeout(function(){ 
            for(var i = 0;i<5;i++){
            generiraj1(ehrIdOseb[1],arr,i,1);  
            generiraj2(ehrIdOseb[1],arr,i,1);
            }
    }, 1000);
    
    setTimeout(function(){ 
            for(var i = 0;i<5;i++){
            generiraj1(ehrIdOseb[2],arr,i,2);
            generiraj2(ehrIdOseb[2],arr,i,2);
            }
    }, 1000);
    
    setTimeout(function() {
        $("#generiraj").empty();
        $("#generiraj").append("<p class='col'><b>Matej Janežič, EhrID: "+ ehrIdOseb[0]+"</b></p></br>"+
        "<p class='col'><b>Patient: Joe Gorge, EhrID: "+ehrIdOseb[1]+"</b></p></br>"+
        "<p class='col'><b>Patient: Alex Naval, EhrID: "+ehrIdOseb[2]+"</b></p>");
    },1000);
}

function izrisiGraf(){
		sessionId = getSessionId();

	var ehrId = $("#ehrIdGraf").val();
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo1").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov1").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				 if (tip == "telesna teža") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
					    		$("#myfirstchart").empty();
					    		izrisi(res, res.length, tip);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo1").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo1").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
				if(tip == "visina"){
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
					    		$("#myfirstchart").empty();
					    		izrisi(res, res.length, tip);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo1").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo1").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
				if(tip == "blood pressure"){
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
					    		$("#myfirstchart").empty();
					    		izrisi(res, res.length, tip);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo1").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo1").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
				
				
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}
 function izrisi(res,value, tip){
 	console.log(value);
 	if(value < 5){
 		alert("Potrebujete vsaj 5 podatkov tipa, ki ga hocete prikazati!");
 		
 	}
    else{
    console.log(res);
    if(tip == "telesna teža"){
    	Morris.Line({
	element: 'myfirstchart',
	data: [
    	{ y: res[value-5].time.substring(0,10), a: res[value-5].weight},
    	{ y: res[value-4].time.substring(0,10), a: res[value-4].weight},
    	{ y: res[value-3].time.substring(0,10), a: res[value-3].weight},
    	{ y: res[value-2].time.substring(0,10), a: res[value-2].weight},
    	{ y: res[value-1].time.substring(0,10), a: res[value-1].weight}
	],
	xkey: 'y',
	ykeys: ['a'],
	xLabels: "day",
	labels: ['Weight(kg)']
		});
	}
	if(tip == "visina"){
		    	Morris.Line({
	element: 'myfirstchart',
	data: [
    	{ y: res[value-5].time.substring(0,10), a: res[value-5].height},
    	{ y: res[value-4].time.substring(0,10), a: res[value-4].height},
    	{ y: res[value-3].time.substring(0,10), a: res[value-3].height},
    	{ y: res[value-2].time.substring(0,10), a: res[value-2].height},
    	{ y: res[value-1].time.substring(0,10), a: res[value-1].height}
	],
	xkey: 'y',
	ykeys: ['a'],
	xLabels: "day",
	labels: ['Height(cm)']
		});
		
	}
	if(tip == "blood pressure"){
		    	Morris.Line({
	element: 'myfirstchart',
	data: [
    	{ y: res[value-5].time.substring(0,10), a: res[value-5].systolic, b: res[value - 5].diastolic},
    	{ y: res[value-4].time.substring(0,10), a: res[value-4].systolic, b: res[value - 4].diastolic },
    	{ y: res[value-3].time.substring(0,10), a: res[value-3].systolic, b: res[value - 3].diastolic },
    	{ y: res[value-2].time.substring(0,10), a: res[value-2].systolic, b: res[value - 2].diastolic },
    	{ y: res[value-1].time.substring(0,10), a: res[value-1].systolic, b: res[value - 1].diastolic }
	],
	xkey: 'y',
	ykeys: ['a', 'b'],
	xLabels: "day",
	labels: ['Height(mm[Hg])', 'Height(mm[Hg])']
		});
		
	}
}
	
}